class AddAttachmentPortfolioPictureToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :portfolio_picture
    end
  end

  def self.down
    remove_attachment :categories, :portfolio_picture
  end
end

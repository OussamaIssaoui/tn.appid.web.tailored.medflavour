class AddInfosToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :short_description, :text
    add_column :categories, :long_description, :text
  end
end

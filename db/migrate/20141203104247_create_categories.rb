class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :name
      t.integer :parent_id, :null => true, :index => true
      t.integer :lft, :null => false, :index => true
      t.integer :rgt, :null => false, :index => true

      # optional fields
      # t.integer :depth, :null => false
      # t.integer :children_count, :null => false
    end
  end

  def self.down
    drop_table :categories
  end
end
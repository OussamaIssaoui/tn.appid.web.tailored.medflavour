class AddAttachmentBannerPictureToRecipes < ActiveRecord::Migration
  def self.up
    change_table :recipes do |t|
      t.attachment :banner_picture
    end
  end

  def self.down
    remove_attachment :recipes, :banner_picture
  end
end

class AddAttachmentProductsPictureToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :products_picture
    end
  end

  def self.down
    remove_attachment :categories, :products_picture
  end
end

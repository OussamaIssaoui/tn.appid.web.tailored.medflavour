class AddAttachmentDownloadToAssets < ActiveRecord::Migration
  def self.up
    change_table :assets do |t|
      t.attachment :download
    end
  end

  def self.down
    remove_attachment :assets, :download
  end
end

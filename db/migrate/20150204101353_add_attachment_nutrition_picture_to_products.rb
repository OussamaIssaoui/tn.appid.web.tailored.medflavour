class AddAttachmentNutritionPictureToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :nutrition_picture
    end
  end

  def self.down
    remove_attachment :products, :nutrition_picture
  end
end

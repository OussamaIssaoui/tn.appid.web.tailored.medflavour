class AddAttachmentPictureToUses < ActiveRecord::Migration
  def self.up
    change_table :uses do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :uses, :picture
  end
end

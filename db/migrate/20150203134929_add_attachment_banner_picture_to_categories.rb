class AddAttachmentBannerPictureToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :banner_picture
    end
  end

  def self.down
    remove_attachment :categories, :banner_picture
  end
end

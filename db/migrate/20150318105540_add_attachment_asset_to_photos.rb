class AddAttachmentAssetToPhotos < ActiveRecord::Migration
  def self.up
    change_table :photos do |t|
      t.attachment :asset
    end
  end

  def self.down
    remove_attachment :photos, :asset
  end
end

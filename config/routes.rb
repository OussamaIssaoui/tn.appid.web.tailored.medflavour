Rails.application.routes.draw do

  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :sessions

  get 'pages/about'
  get 'pages/bottling'
  get 'pages/groves'
  get 'pages/varieties'
  get 'pages/harvesting'
  get 'pages/mill'
  get 'pages/quality'
  get 'pages/contact'

  namespace :media do
    resources :videos, only: [:index, :show]
    resources :albums, only: [:index, :show]
    resources :assets, only: [:index, :show]
    get 'download/:id' => 'assets#download', as: :download
    root 'albums#index'
  end

  namespace :blog do
    resources :posts, only: [:index, :show]
    root 'posts#index'
  end

  namespace :dashboard do
    resources :assets, path: 'downloads'
    resources :videos
    resources :albums
    resources :posts
    resources :products
    resources :categories
    resources :recipes
    resources :uses
    root 'posts#index'
  end

  resources :categories, only: [:index, :show]
  resources :products, only: [:index]
  resources :recipes, only: [:index, :show]
  resources :uses, only: [:index, :show]

  get '/change_locale/:locale', to: 'settings#change_locale', as: :change_locale

  root 'pages#home'
end

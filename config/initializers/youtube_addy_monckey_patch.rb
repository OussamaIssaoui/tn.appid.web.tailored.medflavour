class YouTubeAddy
  extend ActionView::Helpers::TagHelper

  def self.youtube_embed_url(youtube_url)
    vid_id = extract_video_id(youtube_url)
    content_tag(:iframe, nil, src: "//www.youtube.com/embed/#{vid_id}", class: 'embed-responsive-item')
  end
end

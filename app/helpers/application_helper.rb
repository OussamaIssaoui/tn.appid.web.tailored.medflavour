module ApplicationHelper

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end
 
  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(
        content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do 
          concat content_tag(:button, raw('<span aria-hidden="true">&times;</span>'), class: "close", data: { dismiss: 'alert' })
          concat message 
        end
      )
    end
    nil
  end
  
  def first_block_class(index)
    case index % 2
    when 0
      ""
    when 1
      "col-sm-push-5"
    end 
  end

  def second_block_class(index)
    case index % 2
    when 0
      ""
    when 1
      "col-sm-pull-7"
    end 
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields col-sm-10 col-sm-offset-2", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def active_lang(lang)
    if cookies[:medflavour_locale] && I18n.available_locales.include?(cookies[:medflavour_locale].to_sym)
      if cookies[:medflavour_locale] == lang
        "active"
      else
        ""
      end
    else
      ""
    end
  end
end

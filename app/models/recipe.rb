# == Schema Information
#
# Table name: recipes
#
#  id                          :integer          not null, primary key
#  name                        :string(255)
#  description                 :text
#  created_at                  :datetime
#  updated_at                  :datetime
#  picture_file_name           :string(255)
#  picture_content_type        :string(255)
#  picture_file_size           :integer
#  picture_updated_at          :datetime
#  banner_picture_file_name    :string(255)
#  banner_picture_content_type :string(255)
#  banner_picture_file_size    :integer
#  banner_picture_updated_at   :datetime
#  cooking_time                :integer
#  preparation_time            :integer
#

class Recipe < ActiveRecord::Base

  has_attached_file :picture, :styles => { :medium => "300x300>", :thumb => "180x180>" }, :default_url => "/images/:style/missing.png"
  validates_attachment :picture, :presence => true, :content_type => { :content_type => /\Aimage\/.*\Z/ }

  has_attached_file :banner_picture, :styles => { :medium => "300x300>", :thumb => "180x180>" }, :default_url => "/images/:style/missing.png"
  validates_attachment :banner_picture, :presence => true, :content_type => { :content_type => /\Aimage\/.*\Z/ }
  
  validates :name, :description, :cooking_time, :preparation_time, presence: true
end

# == Schema Information
#
# Table name: uses
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  decription           :text
#  created_at           :datetime
#  updated_at           :datetime
#  picture_file_name    :string(255)
#  picture_content_type :string(255)
#  picture_file_size    :integer
#  picture_updated_at   :datetime
#

class Use < ActiveRecord::Base
  has_attached_file :picture, :styles => { :medium => "300x300>", :thumb => "131x131>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
end

# == Schema Information
#
# Table name: albums
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Album < ActiveRecord::Base
  has_many :photos, dependent: :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: proc { |attributes| attributes['description'].blank? }

  validates_presence_of :title

  validate :must_have_one_photo
 
  def must_have_one_photo
    errors.add(:photos, 'must have one photo') if photos_empty?
  end

  def photos_empty?
    photos.empty? or photos.all? {|photo| photo.marked_for_destruction? }
  end

  def self.featured_image(album)
    album.photos.where(featured: true).first ? album.photos.where(featured: true).first : album.photos.first
  end
end

# == Schema Information
#
# Table name: assets
#
#  id                    :integer          not null, primary key
#  name                  :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  download_file_name    :string(255)
#  download_content_type :string(255)
#  download_file_size    :integer
#  download_updated_at   :datetime
#

class Asset < ActiveRecord::Base
  has_attached_file :download, :styles => { :pdf_thumbnail =>["", :png], :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :download, content_type: ['image/jpeg', 'image/png', 'image/gif', 'application/pdf']

  validates_presence_of :name
end

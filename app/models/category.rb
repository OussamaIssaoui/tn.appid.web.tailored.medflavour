# == Schema Information
#
# Table name: categories
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  parent_id                      :integer
#  lft                            :integer          not null
#  rgt                            :integer          not null
#  short_description              :text
#  long_description               :text
#  portfolio_picture_file_name    :string(255)
#  portfolio_picture_content_type :string(255)
#  portfolio_picture_file_size    :integer
#  portfolio_picture_updated_at   :datetime
#  banner_picture_file_name       :string(255)
#  banner_picture_content_type    :string(255)
#  banner_picture_file_size       :integer
#  banner_picture_updated_at      :datetime
#  products_picture_file_name     :string(255)
#  products_picture_content_type  :string(255)
#  products_picture_file_size     :integer
#  products_picture_updated_at    :datetime
#  logo_file_name                 :string(255)
#  logo_content_type              :string(255)
#  logo_file_size                 :integer
#  logo_updated_at                :datetime
#

class Category < ActiveRecord::Base
  acts_as_nested_set
  has_many :products

  has_attached_file :logo, :styles => { :medium => "250x250>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/

  has_attached_file :products_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :products_picture, :content_type => /\Aimage\/.*\Z/

  has_attached_file :portfolio_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :portfolio_picture, :content_type => /\Aimage\/.*\Z/

  has_attached_file :banner_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :banner_picture, :content_type => /\Aimage\/.*\Z/

  validates :name, presence: true

  translates :name, :short_description, :long_description
end

# == Schema Information
#
# Table name: posts
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  body                 :text
#  created_at           :datetime
#  updated_at           :datetime
#  published            :boolean          default(FALSE), not null
#  published_at         :datetime
#  picture_file_name    :string(255)
#  picture_content_type :string(255)
#  picture_file_size    :integer
#  picture_updated_at   :datetime
#  source               :text
#

class Post < ActiveRecord::Base

  has_attached_file :picture, :styles => { :large => "500x500>", :medium => "300x300>", :thumb => "131x131>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/

  validates :name, :body, :picture, presence: true

  include Slugalicious
  slugged :name

  scope :published, -> { where(published: true) }
  
  before_save :check_published_post

  def check_published_post
    if self.published == true
     self.published_at = Time.now
    end
  end
end

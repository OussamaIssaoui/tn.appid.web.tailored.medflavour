# == Schema Information
#
# Table name: products
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  category_id                    :integer
#  created_at                     :datetime
#  updated_at                     :datetime
#  picture_file_name              :string(255)
#  picture_content_type           :string(255)
#  picture_file_size              :integer
#  picture_updated_at             :datetime
#  description                    :text
#  nutrition_picture_file_name    :string(255)
#  nutrition_picture_content_type :string(255)
#  nutrition_picture_file_size    :integer
#  nutrition_picture_updated_at   :datetime
#  details_picture_file_name      :string(255)
#  details_picture_content_type   :string(255)
#  details_picture_file_size      :integer
#  details_picture_updated_at     :datetime
#  row_order                      :integer
#

class Product < ActiveRecord::Base
  belongs_to :category

  has_attached_file :picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/

  has_attached_file :nutrition_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :nutrition_picture, :content_type => /\Aimage\/.*\Z/

  has_attached_file :details_picture, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :details_picture, :content_type => /\Aimage\/.*\Z/

  validates :name, :category_id, presence: true

  include RankedModel
  ranks :row_order, :with_same => :category_id

  translates :name, :description
end

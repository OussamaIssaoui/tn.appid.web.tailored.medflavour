# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  album_id           :integer
#  created_at         :datetime
#  updated_at         :datetime
#  asset_file_name    :string(255)
#  asset_content_type :string(255)
#  asset_file_size    :integer
#  asset_updated_at   :datetime
#  description        :text
#  featured           :boolean
#

class Photo < ActiveRecord::Base
  belongs_to :album

  has_attached_file :asset, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/

  validates :description, length: { maximum: 120 }
end

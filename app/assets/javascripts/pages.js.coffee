# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
  $("section").not('.foot').css height: $(window).height() + "px"
  $(".img-holder").css height: $(window).height() + "px"

  topMasthead = $("#masthead > .top").height()
  remaining_height = parseInt($(window).height() - topMasthead)
  $("#masthead > .bottom").height remaining_height

  $(window).resize ->
    $("section").not('.foot').css height: $(window).height() + "px"
    $(".img-holder").css height: $(window).height() + "px"

    topMasthead = $("#masthead > .top").height()
    remaining_height = parseInt($(window).height() - topMasthead)
    $("#masthead > .bottom").height remaining_height

$(document).ready(ready)
$(document).on('page:load', ready)

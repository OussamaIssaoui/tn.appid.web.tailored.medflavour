# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
  $('.second-nav a').click ->
    $('html, body').animate { scrollTop: 0 }, 'slow'
  $('.navbar').on 'mouseenter', ->
    setTimeout (->
      $('.second-nav').fadeIn()
      return
    ), 300
  $(window).scroll (e) ->
    if $(window).scrollTop() > 100
      $(".second-nav").fadeOut()
    else if $(window).scrollTop() < 100
      $(".second-nav").fadeIn()
    return

$(document).ready(ready)
$(document).on('page:load', ready)
# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
  $(".all").css 'height' : ($(window).height() - 70) + "px"
  $(".product-item").css 'height' : ($(window).height() - 70) + "px"

  $(window).resize ->
    $(".all").css 'height' : ($(window).height() - 70) + "px"
    $(".product-item").css 'height' : ($(window).height() - 70) + "px"

  $('.all a[href*=#]:not([href=#])').click ->
    if location.pathname.replace(/^\//, '') == @pathname.replace(/^\//, '') and location.hostname == @hostname
      target = $(@hash)
      target = if target.length then target else $('[name=' + @hash.slice(1) + ']')
      if target.length
        $('html,body').animate { scrollTop: (target.offset().top - 73) }, 1000
        return false

$(document).ready(ready)
$(document).on('page:load', ready)

# $(window).load ->
#   $('.product-item .row').css 'margin-top', $('.product-item .row').parent().height() - $('.product-item .row').height()
#   $('.pull-down').each ->
#     $(this).css 'margin-top', $(this).parent().height() - $(this).height() - $('.meta').height()
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale
 
  def set_locale
    if cookies[:medflavour_locale] && I18n.available_locales.include?(cookies[:medflavour_locale].to_sym)
      l = cookies[:medflavour_locale].to_sym
    else
      l = I18n.default_locale
      cookies.permanent[:medflavour_locale] = l
    end
    I18n.locale = l
  end

  private

    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end
    helper_method :current_user
    
    def authorize
      redirect_to login_url, alert: "Not authorized" if current_user.nil?
    end
end

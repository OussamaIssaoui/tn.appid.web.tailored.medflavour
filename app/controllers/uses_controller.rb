class UsesController < ApplicationController
  def index
    @uses = Use.all
  end

  def show
    @uses = Use.all
    @use = Use.find(params[:id])
  end
end

class PagesController < ApplicationController

  def home
    @traditional = Category.find_by_name('Oleiva Traditional')
    @premium = Category.find_by_name('Oleiva Premium')
  end

  def about
  end

  def bottling
  end

  def groves
  end

  def varieties
  end

  def harvesting
  end

  def mill
  end

  def quality
  end

  def contact
  end
end

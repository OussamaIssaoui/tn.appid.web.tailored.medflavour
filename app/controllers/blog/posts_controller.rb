class Blog::PostsController < ApplicationController

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.published.order('published_at desc')
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find_from_slug(params[:id])
  end

end

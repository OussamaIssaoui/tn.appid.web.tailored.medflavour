class Media::AssetsController < ApplicationController

  # GET /assets
  # GET /assets.json
  def index
    @assets = Asset.all
  end

  # GET /assets/1
  # GET /assets/1.json
  def show
    @asset = Asset.find(params[:id])
  end

  def download
    @asset = Asset.find(params[:id])
    send_file @asset.download.path,
              :filename => @asset.download_file_name,
              :type => @asset.download_content_type,
              :disposition => 'attachment'
  end

end

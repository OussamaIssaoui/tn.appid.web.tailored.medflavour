# == Schema Information
#
# Table name: categories
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  parent_id                      :integer
#  lft                            :integer          not null
#  rgt                            :integer          not null
#  short_description              :text
#  long_description               :text
#  portfolio_picture_file_name    :string(255)
#  portfolio_picture_content_type :string(255)
#  portfolio_picture_file_size    :integer
#  portfolio_picture_updated_at   :datetime
#  banner_picture_file_name       :string(255)
#  banner_picture_content_type    :string(255)
#  banner_picture_file_size       :integer
#  banner_picture_updated_at      :datetime
#  products_picture_file_name     :string(255)
#  products_picture_content_type  :string(255)
#  products_picture_file_size     :integer
#  products_picture_updated_at    :datetime
#  logo_file_name                 :string(255)
#  logo_content_type              :string(255)
#  logo_file_size                 :integer
#  logo_updated_at                :datetime
#

require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
